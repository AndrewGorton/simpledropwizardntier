package uk.andrewgorton.simpledropwizardntier.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

public class Product {
    private String id;
    private String name;
    private String description;
    private DateTime added;
    private DateTime edited;

    public Product() {

    }

    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty
    public DateTime getAdded() {
        return added;
    }

    public void setAdded(DateTime added) {
        this.added = added;
    }

    @JsonProperty
    public DateTime getEdited() {
        return edited;
    }

    public void setEdited(DateTime edited) {
        this.edited = edited;
    }

}
