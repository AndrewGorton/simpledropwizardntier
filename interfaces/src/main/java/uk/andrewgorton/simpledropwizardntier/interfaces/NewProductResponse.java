package uk.andrewgorton.simpledropwizardntier.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NewProductResponse {
    private String id;

    public NewProductResponse() {

    }

    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
