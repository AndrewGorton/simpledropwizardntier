package uk.andrewgorton.simpledropwizardntier.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Products {
    private List<Product> products = new ArrayList<Product>();
    private List<String> servers;

    public Products() {

    }

    @JsonProperty
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @JsonProperty
    public List<String> getServers() {
        return servers;
    }

    public void setServers(List<String> servers) {
        this.servers = servers;
    }
}
