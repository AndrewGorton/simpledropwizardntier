package uk.andrewgorton.simpledropwizardntier.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateProduct {
    private String name;
    private String description;

    public UpdateProduct() {

    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
