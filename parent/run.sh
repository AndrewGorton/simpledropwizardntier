#!/bin/bash
mvn clean install 

pushd ../backend/docker
./build.sh
popd

pushd ../frontend/docker
./build.sh
popd

