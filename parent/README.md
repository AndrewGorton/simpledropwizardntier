# README

This is the parent module which should be able to build and run all the child projects

See README.md in parent directory for more information.

## Version Numbers

Next version number bump should be:-

```bash
mvn versions:set -DnewVersion=1.0.3
```

Remember to update the Procfile when you bump numbers!
