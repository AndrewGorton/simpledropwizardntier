#!/bin/bash

docker ps -a | grep 'Exited' | awk '{print $1}' | xargs docker rm

rm -rf content
mkdir content
cp ../target/backend-1.0.3-SNAPSHOT.jar content
cp ../configuration/dev.yml content

cat > DOCKERFILE << EOF
FROM dockerfile/java:openjdk-7-jre
MAINTAINER Andrew Gorton <noreply@andrewgorton.uk>

RUN mkdir -p /opt/backend

ADD content/backend-1.0.3-SNAPSHOT.jar /opt/backend/
ADD content/dev.yml /opt/backend/

EXPOSE 8020 8021

CMD ["/usr/bin/java", "-jar", "/opt/backend/backend-1.0.3-SNAPSHOT.jar", "server", "/opt/backend/dev.yml"]
EOF

tar cvfz build.tgz Dockerfile content

docker build -t andrewgorton/sdwntier-backend - < build.tgz

echo 'Try docker run -p 8020:8020 -p 8021:8021 --name backend --rm -it andrewgorton/sdwntier-backend'
echo "Then open http://$(/usr/local/bin/boot2docker ip 2>/dev/null):8020/products"
