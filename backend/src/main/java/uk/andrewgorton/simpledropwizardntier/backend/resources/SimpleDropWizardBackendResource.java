package uk.andrewgorton.simpledropwizardntier.backend.resources;

import com.codahale.metrics.annotation.Timed;
import org.joda.time.DateTime;
import uk.andrewgorton.simpledropwizardntier.interfaces.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
public class SimpleDropWizardBackendResource {
    private static final Object LOCK_OBJECT = new Object();
    private static Products products;

    // Static constructor
    {
        products = new Products();
        Product singleProduct = new Product();
        singleProduct.setId("5a6abf8d-6bf4-458b-a50e-d0ace63bbf5c");
        singleProduct.setName("Box spanner");
        singleProduct.setDescription("A simple box spanner. 19mm. Unbreakable!");
        singleProduct.setAdded(DateTime.now());
        singleProduct.setEdited(singleProduct.getAdded());
        products.getProducts().add(singleProduct);

        singleProduct = new Product();
        singleProduct.setId("d4f0d4c1-2a4b-4909-9f57-3ca87c127648");
        singleProduct.setName("100 RJ45 crimp plugs");
        singleProduct.setDescription("Bag of 100 crimp plugs. Suitable for use with stranded and solid Cat5e+ cables");
        singleProduct.setAdded(DateTime.now());
        singleProduct.setEdited(singleProduct.getAdded());
        products.getProducts().add(singleProduct);
    }

    public SimpleDropWizardBackendResource() {

    }

    private List<String> getServers() {
        List<String> servers = new ArrayList<String>();
        try {
            servers.add(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            try {
                servers.add(InetAddress.getLocalHost().getHostAddress());
            } catch (UnknownHostException e2) {
                servers.add("[unknown]");
            }
        }
        return servers;
    }

    @GET
    @Timed
    public Products getProducts() {
        List<String> servers = getServers();

        synchronized (LOCK_OBJECT) {
            products.setServers(servers);
            return products;
        }
    }

    @Path("/{id}")
    @GET
    @Timed
    public Response getProduct(@PathParam("id") String id) {
        synchronized (LOCK_OBJECT) {
            for (Product sp : products.getProducts()) {
                if (sp.getId().compareToIgnoreCase(id) == 0) {
                    return Response.ok(sp).build();
                }
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @POST
    @Timed
    public NewProductResponse createNewProduct(NewProductRequest request) {
        Product newProduct = new Product();
        newProduct.setId(UUID.randomUUID().toString());
        newProduct.setName(request.getName());
        newProduct.setDescription(request.getDescription());
        newProduct.setAdded(DateTime.now());
        newProduct.setEdited(newProduct.getAdded());

        synchronized (LOCK_OBJECT) {
            products.getProducts().add(newProduct);
        }

        NewProductResponse result = new NewProductResponse();
        result.setId(newProduct.getId());
        return result;
    }

    @Path("/{id}")
    @PUT
    @Timed
    public Response updateProduct(@PathParam("id") String id, UpdateProduct updateProduct) {
        synchronized (LOCK_OBJECT) {
            for (Product sp : products.getProducts()) {
                if (sp.getId().compareToIgnoreCase(id) == 0) {
                    sp.setName(updateProduct.getName());
                    sp.setDescription(updateProduct.getDescription());
                    sp.setEdited(DateTime.now());
                    return Response.ok(sp).build();
                }
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }


    @Path("/{id}")
    @DELETE
    @Timed
    public Response deleteProduct(@PathParam("id") String id) {
        synchronized (LOCK_OBJECT) {
            for (Product sp : products.getProducts()) {
                if (sp.getId().compareToIgnoreCase(id) == 0) {
                    products.getProducts().remove(sp);
                    return Response.ok(sp).build();
                }
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
