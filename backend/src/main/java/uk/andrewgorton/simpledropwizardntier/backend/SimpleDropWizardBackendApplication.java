package uk.andrewgorton.simpledropwizardntier.backend;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import uk.andrewgorton.simpledropwizardntier.backend.configuration.SimpleDropWizardBackendConfiguration;
import uk.andrewgorton.simpledropwizardntier.backend.resources.SimpleDropWizardBackendResource;

public class SimpleDropWizardBackendApplication extends Application<SimpleDropWizardBackendConfiguration> {
    public static void main(String[] args) throws Exception {
        new SimpleDropWizardBackendApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<SimpleDropWizardBackendConfiguration> bootstrap) {

    }

    @Override
    public void run(SimpleDropWizardBackendConfiguration configuration,
                    Environment environment) {
        final SimpleDropWizardBackendResource resource = new SimpleDropWizardBackendResource();
        environment.jersey().register(resource);
    }
}
