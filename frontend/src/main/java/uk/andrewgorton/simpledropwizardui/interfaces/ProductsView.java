package uk.andrewgorton.simpledropwizardui.interfaces;

import io.dropwizard.views.View;
import uk.andrewgorton.simpledropwizardntier.interfaces.Products;

public class ProductsView extends View {
    private Products products;

    public ProductsView(Products products) {
        super("products.mustache");
        this.products = products;
    }

    public Products getProducts() {
        return products;
    }

}
