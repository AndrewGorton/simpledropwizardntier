package uk.andrewgorton.simpledropwizardui;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import io.dropwizard.views.ViewBundle;
import uk.andrewgorton.simpledropwizardui.configuration.SimpleDropWizardUiConfiguration;
import uk.andrewgorton.simpledropwizardui.resources.HtmlView;
import uk.andrewgorton.simpledropwizardui.resources.RestApi;

public class SimpleDropWizardUiApplication extends Application<SimpleDropWizardUiConfiguration> {
    public static void main(String[] args) throws Exception {
        new SimpleDropWizardUiApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<SimpleDropWizardUiConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle());
        bootstrap.addBundle(new AssetsBundle("/assets/css", "/css", null, "css"));
        bootstrap.addBundle(new AssetsBundle("/assets/js", "/js", null, "js"));
        bootstrap.addBundle(new AssetsBundle("/assets/html", "/html", null, "html"));
    }

    @Override
    public void run(SimpleDropWizardUiConfiguration configuration,
                    Environment environment) {
        final HtmlView resource = new HtmlView();
        environment.jersey().register(resource);
        final RestApi restapi = new RestApi();
        environment.jersey().register(restapi);

    }
}
