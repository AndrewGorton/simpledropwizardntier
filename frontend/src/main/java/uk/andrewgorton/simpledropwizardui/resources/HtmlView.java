package uk.andrewgorton.simpledropwizardui.resources;

import com.codahale.metrics.annotation.Timed;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import uk.andrewgorton.simpledropwizardntier.interfaces.Products;
import uk.andrewgorton.simpledropwizardui.interfaces.ProductsView;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/products")
@Produces(MediaType.TEXT_HTML)
public class HtmlView {
    public HtmlView() {

    }

    @GET
    @Timed
    public ProductsView getProducts() {
        Client c = new Client();
        WebResource r = c.resource(String.format("http://%s:%s/products",
                System.getenv("BACKEND_PORT_8020_TCP_ADDR"),
                System.getenv("BACKEND_PORT_8020_TCP_PORT")));
        Products products = r.accept(MediaType.APPLICATION_JSON_TYPE).get(Products.class);
        return new ProductsView(products);
    }
}
