package uk.andrewgorton.simpledropwizardui.resources;

import com.codahale.metrics.annotation.Timed;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import uk.andrewgorton.simpledropwizardntier.interfaces.NewProductRequest;
import uk.andrewgorton.simpledropwizardntier.interfaces.NewProductResponse;
import uk.andrewgorton.simpledropwizardntier.interfaces.Products;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rest/products")
@Produces(MediaType.APPLICATION_JSON)
public class RestApi {
    public RestApi() {

    }

    @GET
    @Timed
    public Products getProducts() {
        Client c = new Client();
        WebResource r = c.resource(String.format("http://%s:%s/products",
                System.getenv("BACKEND_PORT_8020_TCP_ADDR"),
                System.getenv("BACKEND_PORT_8020_TCP_PORT")));
        return r.accept(MediaType.APPLICATION_JSON_TYPE).get(Products.class);
    }

    @POST
    @Timed
    public NewProductResponse newProduct(NewProductRequest req) {
        Client c = new Client();
        WebResource r = c.resource(String.format("http://%s:%s/products",
                System.getenv("BACKEND_PORT_8020_TCP_ADDR"),
                System.getenv("BACKEND_PORT_8020_TCP_PORT")));
        return r.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_TYPE).post(NewProductResponse.class, req);
    }

    @DELETE
    @Path("/{id}")
    @Timed
    public Response deleteProduct(@PathParam("id") String id) {
        Client c = new Client();
        WebResource r = c.resource(String.format("http://%s:%s/products/%s",
                System.getenv("BACKEND_PORT_8020_TCP_ADDR"),
                System.getenv("BACKEND_PORT_8020_TCP_PORT"), id));
        ClientResponse cr = r.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON_TYPE).delete(ClientResponse.class);
        return Response.status(cr.getStatus()).build();
    }
}
