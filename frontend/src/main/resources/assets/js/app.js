var app = angular.module('SimpleDropWizardNTier',[]);

app.factory("broadcastService", function($rootScope) {
  var broadcaster = {};

  broadcaster.broadcast = function() {
    $rootScope.$broadcast('handleRefresh');
  }

  return broadcaster;
});