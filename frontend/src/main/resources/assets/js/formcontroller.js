function FormController($scope, $http, broadcastService) {

    $scope.submitForm = function() {
        $http({ method: 'POST', url: '/rest/products', data: $scope.newproduct }).
            success(function (data, status, headers, config) {
                if(status == 200) {
                    $scope.newproduct = {};
                    broadcastService.broadcast();
                    //location.reload();
                }
            }).
            error(function (data, status, headers, config) {
                throw "Failed to upload";
            });
     }

    $scope.init = function() {

    }

    $scope.init();
};

app.controller('FormController', ['$scope', '$http', 'broadcastService', FormController]);