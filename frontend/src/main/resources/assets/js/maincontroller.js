function MainController($scope, $http) {
    $scope.getData = function() {
        $http({ method: 'GET', url: '/rest/products' }).
            success(function (data, status, headers, config) {
                $scope.products = data;
            }).
            error(function (data, status, headers, config) {
                throw "Failed to download products";
            });
    }

    $scope.handleDelete = function(id) {
        $http({ method: 'DELETE', url: '/rest/products/' + id }).
            success(function (data, status, headers, config) {
                $scope.getData();
            }).
            error(function (data, status, headers, config) {
                throw "Failed to download products";
            });
    }

    $scope.init = function() {
        $scope.$on("handleRefresh", function (event, args) {
            $scope.getData();
        });
        $scope.getData();
    }

    $scope.init();
};

app.controller('MainController', ['$scope', '$http', MainController]);