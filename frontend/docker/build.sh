#!/bin/bash

docker ps -a | grep 'Exited' | awk '{print $1}' | xargs docker rm

rm -rf content
mkdir content
cp ../target/frontend-1.0.3-SNAPSHOT.jar content
cp ../configuration/dev.yml content

cat > DOCKERFILE << EOF
FROM dockerfile/java:openjdk-7-jre
MAINTAINER Andrew Gorton <noreply@andrewgorton.uk>

RUN mkdir -p /opt/frontend

ADD content/frontend-1.0.3-SNAPSHOT.jar /opt/frontend/
ADD content/dev.yml /opt/frontend/

EXPOSE 8030 8031

CMD ["/usr/bin/java", "-jar", "/opt/frontend/frontend-1.0.3-SNAPSHOT.jar", "server", "/opt/frontend/dev.yml"]
EOF

tar cvfz build.tgz Dockerfile content
docker build -t andrewgorton/sdwntier-frontend - < build.tgz

echo 'Try docker run -p 8030:8030 -p 8031:8031 --name frontend --link backend:backend --rm -it andrewgorton/sdwntier-frontend'
echo "Then open http://$(/usr/local/bin/boot2docker ip 2>/dev/null):8030/html/index.html"
