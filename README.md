# README.md

This is a multi-module project using DropWizard as an example N-Tier application.

It's a simple product catalogue with no serious persistence.

The Interfaces module defines the messages between the Frontend and Backend project.

The Backend provides a simple REST/JSON API for retrieving and updating the product listing.

The Frontend places an AngularJS frontend and calls the backend to perform operations.

## Build Environment
```bash
$ mvn --version
Apache Maven 3.1.1 (0728685237757ffbf44136acec0402957f723d9a; 2013-09-17 16:22:22+0100)
Maven home: /opt/boxen/homebrew/Cellar/maven/3.1.1/libexec
Java version: 1.8.0_20, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_20.jdk/Contents/Home/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.9.5", arch: "x86_64", family: "mac"

$ docker version
Client version: 1.1.2
Client API version: 1.13
Go version (client): go1.2.1
Git commit (client): d84a070
Server version: 1.2.0
Server API version: 1.14
Go version (server): go1.3.1
Git commit (server): fa7b24f
```

## Building
```bash
$ cd parent
$ mvn clean install
$ pushd ../backend/docker
$ ./build.sh
$ popd
$ pushd ../frontend/docker
$ ./build.sh
$ popd
```

## Running

```bash
$ docker run -p 8020:8020 -p 8021:8021 --name backend --rm -it andrewgorton/sdwntier-backend
$ docker run -p 8030:8030 -p 8031:8031 --name frontend --link backend:backend --rm -it andrewgorton/sdwntier-frontend
```

which starts the frontend service on port 8030 (`http://<docker_host>:8030/html/index.html`) and the backend on port 8020 (`http://<docker_host>:8020/products`).

## License

See the enclosed LICENSE file.
